
This is a tutorial modification



Ansible
=======

Ansible is a radically simple IT automation system. It handles configuration management, application deployment, cloud provisioning, ad-hoc task execution, network automation, and multi-node orchestration. Ansible makes complex changes like zero-downtime rolling updates with load balancers easy. More information on the Ansible website.

For this playbook would you use ansible's version 2.7.12. 

- To update ansible version follow those steps:

$ sudo apt-get install software-properties-common

$ sudo apt-add-repository ppa:ansible/ansible

$ sudo apt-get update

$ sudo apt-get install ansible

infra challenge
===============

A playbook used to automate instalation and configuration of flaskr in ubuntu16.04

Requirements
------------

 - {{host}} : Host to set where to install flaskr
 - {{user}} : User on host
 - {{dest_dir_owner}} : Owner of flaskr directory
 - {{dest_dir_group}} : Group of flaskr directory
 - {{dest_dir_perm}} : Permission to access to faskr directory
 
Example of ansible command to execute the playbook
--------------------------------------------------

- ansible-playbook -i hosts infra_challenge.yml --extra-vars "host=localhost user=ahmed dest_dir_owner=ahmed dest_dir_group=ahmed dest_dir_perm=777" 
- PS: You have to update ansible_sudo_pass='*******' in hosts file with user's password for sudo tasks (TODO : encrypt the password with ansible-vault).

Playbook Variables
------------------

- {{dest_dir}}: Directory where to clone the git repo
 
- {{gh_repo}}: Flasker's git repo

roles
-----

- Install flaskr : Installing dependencies and flaskr from repo https://github.com/silshack/flaskr and configuring repo rights access

- Configuring flaskr : Confiiguring python script as demanded in a test

- Configuring supervisor : Installing and configuring supervisor to start flaskr script at boot of the host


Author Information
------------------

Ahmed GHANMI

